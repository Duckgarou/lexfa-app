(function(){
	'use strict';
	angular
		.module('app.roster')
		.controller('RosterJuvenilController', RosterJuvenilController);

		RosterJuvenilController.$inject = ['getRoster', 'config', 'loading'];

		function RosterJuvenilController(getRoster, config, loading){
			var vm = this;
			vm.rosterCategoryId = config.rosterJuvenilCategoryId;
			vm.roster = [];
			loading.showLoadingOverlay();
			activate(vm.rosterCategoryId);

			function activate(pageId){
				return getRoster.getRosterList(pageId).then(onPageDetailsComplete, onError);
			}
			
			function onPageDetailsComplete(response) {
				vm.roster = response;
				loading.hideLoadingOverlay();
				console.log('Got Page Details');
				console.log(vm.roster);
	        }

			function onError(err){
	            vm.error = 'Sorry Mario, but the princess is in another castle =( ...';
				console.log(vm.error);
			}
		}
})();