(function(){
	'use strict';
	angular
		.module('app.roster')
		.controller('RosterDetalleController', RosterDetalleController);

		RosterDetalleController.$inject = ['getRoster', 'config', '$stateParams', 'loading'];

		function RosterDetalleController(getRoster, config, $stateParams, loading){
			var vm = this;
			vm.rosterID = $stateParams.rosterID;
			vm.roster = [];
			loading.showLoadingOverlay();
			activate(vm.rosterID);

			function activate(pageId){
				return getRoster.getRosterDetails(pageId).then(onPageDetailsComplete, onError);
			}
			
			function onPageDetailsComplete(response) {
				vm.roster = response;
				loading.hideLoadingOverlay();
				console.log('Got Page Details');
				console.log(vm.roster);
	        }

			function onError(err){
	            vm.error = 'Sorry Mario, but the princess is in another castle =( ...';
				console.log(vm.error);
			}
		}
})();