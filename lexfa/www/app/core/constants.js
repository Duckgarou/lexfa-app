 (function(){
	'use strict';
	angular
		.module('app.core')
		.constant('config',{
			calendarioCategoryId : 325,
			calendarioFemenilCategoryId : 333,
			calendarioMasterCategoryId : 342,
			calendarioSeniorCategoryId : 370,
			calendarioJuvenilCategoryId : 360,
			estadisticasId: 3749,
			estadisticasFemenilId: 4977,
			estadisticasMasterId: 6656,
			estadisticasSeniorId: 10149,
			estadisticasJuvenilId: 7976,
			noticiasCategoryId: 31,
			rosterCategoryId: 323,
			rosterFemenilCategoryId: 334,
			rosterMasterCategoryId: 343,
			rosterSeniorCategoryId: 372,
			rosterJuvenilCategoryId: 361,
			galeriaCategoryId: 324
		});
})();
