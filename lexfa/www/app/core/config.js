(function(){
    'use strict';
    angular.module('app.core')
        .run(function($ionicPlatform) {
            $ionicPlatform.ready(function() {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                if (window.cordova && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                    cordova.plugins.Keyboard.disableScroll(true);

                }
                if (window.StatusBar) {
                    // org.apache.cordova.statusbar required
                    StatusBar.styleDefault();
                }
            });
        })
        .config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
            $stateProvider
                .state('app', {
                    url: '/app',
                    abstract: true,
                    templateUrl: 'app/layout/menu.html'
                })
                .state('app.home', {
                    url: '/home',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/home/home.html'
                        }
                    }
                })
                .state('app.noticias', {
                    url: '/noticias',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/noticias/noticias.html'
                        }
                    }
                })
                .state('app.noticias-detalle', {
                    url: '/noticias-detalle/:noticiaID',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/noticias/noticias-detalle.html'
                        }
                    }
                })
                .state('app.calendario-home', {
                    url: '/calendario-home',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/calendario/calendario-home.html'
                        }
                    }
                })
                .state('app.calendario', {
                    url: '/calendario',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/calendario/calendario.html'
                        }
                    }
                })
                .state('app.calendario-femenil', {
                    url: '/calendario-femenil',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/calendario/calendario-femenil.html'
                        }
                    }
                })
                .state('app.calendario-juvenil', {
                    url: '/calendario-juvenil',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/calendario/calendario-juvenil.html'
                        }
                    }
                })
                .state('app.calendario-master', {
                    url: '/calendario-master',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/calendario/calendario-master.html'
                        }
                    }
                })
                .state('app.calendario-senior', {
                    url: '/calendario-senior',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/calendario/calendario-senior.html'
                        }
                    }
                })
                .state('app.calendario-detalle', {
                    url: '/calendario-detalle/:calendarioID',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/calendario/calendario-detalle.html'
                        }
                    }
                })
                .state('app.estadisticas-home', {
                    url: '/estadisticas-home',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/estadisticas/estadisticas-home.html'
                        }
                    }
                })
                .state('app.estadisticas', {
                    url: '/estadisticas',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/estadisticas/estadisticas.html'
                        }
                    }
                })
                .state('app.estadisticas-femenil', {
                    url: '/estadisticas-femenil',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/estadisticas/estadisticas-femenil.html'
                        }
                    }
                })
                .state('app.estadisticas-master', {
                    url: '/estadisticas-master',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/estadisticas/estadisticas-master.html'
                        }
                    }
                })
                .state('app.estadisticas-senior', {
                    url: '/estadisticas-senior',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/estadisticas/estadisticas-senior.html'
                        }
                    }
                })
                .state('app.estadisticas-juvenil', {
                    url: '/estadisticas-juvenil',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/estadisticas/estadisticas-juvenil.html'
                        }
                    }
                })
                .state('app.roster-home', {
                    url: '/roster-home',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/roster/roster-home.html'
                        }
                    }
                })
                .state('app.roster', {
                    url: '/roster',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/roster/roster.html'
                        }
                    }
                })
                .state('app.roster-femenil', {
                    url: '/roster-femenil',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/roster/roster-femenil.html'
                        }
                    }
                })
                .state('app.roster-juvenil', {
                    url: '/roster-juvenil',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/roster/roster-juvenil.html'
                        }
                    }
                })
                .state('app.roster-master', {
                    url: '/roster-master',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/roster/roster-master.html'
                        }
                    }
                })
                .state('app.roster-senior', {
                    url: '/roster-senior',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/roster/roster-senior.html'
                        }
                    }
                })
                .state('app.roster-detalle', {
                    url: '/roster-detalle/:rosterID',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/roster/roster-detalle.html'
                        }
                    }
                })
                .state('app.galeria', {
                    url: '/galeria',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/galeria/galeria.html'
                        }
                    }
                })
                .state('app.galeria-detalle', {
                    url: '/galeria-detalle/:galeriaID',
                    views: {
                        'menuContent': {
                          templateUrl: 'app/galeria/galeria-detalle.html'
                        }
                    }
                });
            $urlRouterProvider.otherwise('/app/home');
        });
})();
