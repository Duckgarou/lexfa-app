(function(){
	'use strict';
	angular
		.module('app.core')
		.factory('getCalendar', getCalendar);

		function getCalendar($http){
			var getCalendarList = function(calendarCategoryID){
				return $http.get('http://arenafootball.com.mx/wp-json/wp/v2/posts?categories='+calendarCategoryID+'&per_page=100')
							.then(function(response){
								console.log(calendarCategoryID);
								return response.data;
							});
			};
			var getCalendarDetails = function(calendarId){
				return $http.get('http://arenafootball.com.mx/wp-json/wp/v2/posts/'+calendarId)
							.then(function(response){
								return response.data;
							});
			};
			return {
				getCalendarList: getCalendarList,
				getCalendarDetails: getCalendarDetails
			}
		}
})();