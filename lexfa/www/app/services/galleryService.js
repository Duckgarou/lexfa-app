(function(){
	'use strict';
	angular
		.module('app.core')
		.factory('getGallery', getGallery);

		function getGallery($http){
			var getGalleryList = function(galleryCategoryID){
				return $http.get('http://arenafootball.com.mx/wp-json/wp/v2/posts?categories='+galleryCategoryID+'&per_page=100')
							.then(function(response){
								console.log(galleryCategoryID);
								return response.data;
							});
			};
			var getGalleryDetails = function(galleryId){
				return $http.get('http://arenafootball.com.mx/wp-json/wp/v2/posts/'+galleryId)
							.then(function(response){
								return response.data;
							});
			};
			return {
				getGalleryList: getGalleryList,
				getGalleryDetails: getGalleryDetails
			}
		}
})();