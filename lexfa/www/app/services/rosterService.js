(function(){
	'use strict';
	angular
		.module('app.core')
		.factory('getRoster', getRoster);

		function getRoster($http){
			var getRosterList = function(rosterCategoryID){
				return $http.get('http://arenafootball.com.mx/wp-json/wp/v2/posts?categories='+rosterCategoryID+'&per_page=100')
							.then(function(response){
								console.log(rosterCategoryID);
								return response.data;
							});
			};
			var getRosterDetails = function(rosterId){
				return $http.get('http://arenafootball.com.mx/wp-json/wp/v2/posts/'+rosterId)
							.then(function(response){
								return response.data;
							});
			};
			return {
				getRosterList: getRosterList,
				getRosterDetails: getRosterDetails
			}
		}
})();