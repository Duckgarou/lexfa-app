(function(){
	'use strict';
	angular
		.module('app.core')
		.factory('getContent', getContent);

		function getContent($http){
			var getPageContent = function(pageId){
				return $http.get('http://arenafootball.com.mx/wp-json/wp/v2/posts/'+pageId)
							.then(function(response){
								return response.data;
							});
			}
			return {
				getPageContent: getPageContent
			}
		}
})();