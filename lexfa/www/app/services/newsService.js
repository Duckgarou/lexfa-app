(function(){
	'use strict';
	angular
		.module('app.core')
		.factory('getNews', getNews);

		function getNews($http){
			var getNewsList = function(newsCategoryID){
				return $http.get('http://arenafootball.com.mx/wp-json/wp/v2/posts?categories='+newsCategoryID)
							.then(function(response){
								console.log(newsCategoryID);
								return response.data;
							});
			};
			var getNewsDetails = function(newsId){
				return $http.get('http://arenafootball.com.mx/wp-json/wp/v2/posts/'+newsId)
							.then(function(response){
								return response.data;
							});
			};
			return {
				getNewsList: getNewsList,
				getNewsDetails: getNewsDetails
			}
		}
})();