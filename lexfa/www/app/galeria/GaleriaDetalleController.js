(function(){
	'use strict';
	angular
		.module('app.galeria')
		.controller('GaleriaDetalleController', GaleriaDetalleController);

		GaleriaDetalleController.$inject = ['getGallery', 'config', '$stateParams', 'loading'];

		function GaleriaDetalleController(getGallery, config, $stateParams, loading){
			var vm = this;
			var galeriaID = $stateParams.galeriaID;
			vm.galeriaID = galeriaID;
			vm.gallery = [];
			loading.showLoadingOverlay();
			activate(galeriaID);

			function activate(pageId){
				return getGallery.getGalleryDetails(pageId).then(onPageDetailsComplete, onError);
			}
			
			function onPageDetailsComplete(response) {
				vm.gallery = response;
				loading.hideLoadingOverlay();
				console.log('Got Page Details');
				console.log(vm.gallery);
	        }

			function onError(err){
	            vm.error = 'Sorry Mario, but the princess is in another castle =( ...';
				console.log(vm.error);
			}

			$(document).on('click', '.gallery .gallery-content img', function(){
				var imgUrl = $(this).attr('src');
				console.log(imgUrl);
				$('.gallery').append('<div class="gallery-overlay"><div class="gallery-overlay-inner"><a href="javascript:void(0)">X</a><img src="'+imgUrl+'"></div></div>');
			});
			$(document).on('click', '.gallery-overlay, .button', function(){
				$('.gallery-overlay').detach();
			});
		}

})();