(function(){
	'use strict';
	angular
		.module('app.galeria')
		.controller('GaleriaController', GaleriaController);

		GaleriaController.$inject = ['getGallery', 'config', 'loading'];

		function GaleriaController(getGallery, config, loading){
			var vm = this;
			var calendarioCategoriaID = config.galeriaCategoryId;
			vm.calendarioCategoriaID = calendarioCategoriaID;
			vm.galeria = [];
			loading.showLoadingOverlay();
			activate(calendarioCategoriaID);

			function activate(pageId){
				return getGallery.getGalleryList(pageId).then(onPageDetailsComplete, onError);
			}
			
			function onPageDetailsComplete(response) {
				vm.galeria = response;
				loading.hideLoadingOverlay();
				console.log('Got Page Details');
				console.log(vm.galeria);
	        }

			function onError(err){
	            vm.error = 'Sorry Mario, but the princess is in another castle =( ...';
				console.log(vm.error, err);
			}
		}

})();