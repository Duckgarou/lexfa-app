(function(){
	'use strict';
	angular
		.module('app.calendario')
		.controller('CalendarioDetalleController', CalendarioDetalleController);

		CalendarioDetalleController.$inject = ['$stateParams', 'getCalendar', 'config', 'loading'];

		function CalendarioDetalleController($stateParams, getCalendar, config, loading){
			var vm = this;

			vm.calendarioID = $stateParams.calendarioID;
			vm.roster = [];
			loading.showLoadingOverlay();
			activate(vm.calendarioID);


			function activate(pageId){
				return getCalendar.getCalendarDetails(pageId).then(onPageDetailsComplete, onError);
			}
			
			function onPageDetailsComplete(response) {
				vm.calendarDetails = response;
				loading.hideLoadingOverlay();
				console.log('Got Page Details');
				console.log(vm.calendarDetails);
	        }

			function onError(err){
	            vm.error = 'Sorry Mario, but the princess is in another castle =( ...';
				console.log(vm.error);
			}

		}

})();