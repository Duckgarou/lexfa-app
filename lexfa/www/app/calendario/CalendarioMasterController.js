(function(){
	'use strict';
	angular
		.module('app.calendario')
		.controller('CalendarioMasterController', CalendarioMasterController);

		CalendarioMasterController.$inject = ['$stateParams', 'getCalendar', 'config', 'loading'];

		function CalendarioMasterController($stateParams, getCalendar, config, loading){
			var vm = this;
			vm.calendarioCategoryId = config.calendarioMasterCategoryId;
			vm.calendar = [];
			loading.showLoadingOverlay();
			activate(vm.calendarioCategoryId);

			function activate(pageId){
				return getCalendar.getCalendarList(pageId).then(onPageDetailsComplete, onError);
			}
			
			function onPageDetailsComplete(response) {
				vm.calendar = response;
				loading.hideLoadingOverlay();
				console.log('Got Calendar Details');
				console.log(vm.calendar);
	        }

			function onError(err){
	            vm.error = 'Sorry Mario, but the princess is in another castle =( ...';
				console.log(vm.error, err);
			}

		}

})();