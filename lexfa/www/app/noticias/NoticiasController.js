(function(){
	'use strict';
	angular
		.module('app.noticias')
		.controller('NoticiasController', NoticiasController);

		NoticiasController.$inject = ['getNews', 'config', 'loading'];

		function NoticiasController(getNews, config, loading){
			var vm = this;
			vm.noticiasCategoryId = config.noticiasCategoryId;
			vm.noticias = [];
			loading.showLoadingOverlay();
			activate(vm.noticiasCategoryId);

			function activate(pageId){
				return getNews.getNewsList(pageId).then(onPageDetailsComplete, onError);
			}
			
			function onPageDetailsComplete(response) {
				vm.noticias = response;
				loading.hideLoadingOverlay();
				console.log('Got Page Details');
				console.log(vm.noticias);
	        }

			function onError(err){
	            vm.error = 'Sorry Mario, but the princess is in another castle =( ...';
				console.log(vm.error);
			}
		}
})();