(function(){
	'use strict';
	angular
		.module('app.noticias')
		.controller('NoticiaDetalleController', NoticiaDetalleController);

		NoticiaDetalleController.$inject = ['getNews', 'config', '$stateParams', 'loading'];

		function NoticiaDetalleController(getNews, config, $stateParams, loading){
			var vm = this;
			vm.noticiaID = $stateParams.noticiaID;
			vm.noticia = [];
			loading.showLoadingOverlay();
			activate(vm.noticiaID);

			function activate(pageId){
				return getNews.getNewsDetails(pageId).then(onPageDetailsComplete, onError);
			}
			
			function onPageDetailsComplete(response) {
				vm.noticia = response;
				loading.hideLoadingOverlay();
				console.log('Got Page Details');
				console.log(vm.noticia);
	        }

			function onError(err){
	            vm.error = 'Sorry Mario, but the princess is in another castle =( ...';
				console.log(vm.error);
			}
		}
})();