(function(){
    'use strict';
    angular.module('app',[
		/* Everybody has access to these */
        'app.core',

        /* Feature areas */
        'app.calendario',
        'app.estadisticas',
        'app.noticias',
        'app.roster',
        'app.galeria'
    ]);
})();