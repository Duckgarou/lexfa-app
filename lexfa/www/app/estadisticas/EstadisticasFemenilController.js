(function(){
	'use strict';
	angular
		.module('app.estadisticas')
		.controller('EstadisticasFemenilController', EstadisticasFemenilController);

		EstadisticasFemenilController.$inject = ['getContent', 'config', 'loading'];

		function EstadisticasFemenilController(getContent, config, loading){
			var vm = this;
			var pageId = config.estadisticasFemenilId;
			vm.pageId = pageId;
			vm.pageDetails = [];
			loading.showLoadingOverlay();
			activate(pageId);

			function activate(pageId){
				return getContent.getPageContent(pageId).then(onPageDetailsComplete, onError);
			}
			
			function onPageDetailsComplete(response) {
				vm.pageDetails = response;
				loading.hideLoadingOverlay();
				console.log('Got Page Details');
				console.log(vm.pageDetails);
	        }

			function onError(err){
	            vm.error = 'Sorry Mario, but the princess is in another castle =( ...';
				console.log(vm.error, err);
			}

		}

})();