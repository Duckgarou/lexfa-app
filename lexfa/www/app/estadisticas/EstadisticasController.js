(function(){
	'use strict';
	angular
		.module('app.estadisticas')
		.controller('EstadisticasController', EstadisticasController);

		EstadisticasController.$inject = ['getContent', 'config', 'loading'];

		function EstadisticasController(getContent, config, loading){
			var vm = this;
			var pageId = config.estadisticasId;
			vm.pageId = pageId;
			vm.pageDetails = [];
			loading.showLoadingOverlay();
			activate(pageId);

			function activate(pageId){
				return getContent.getPageContent(pageId).then(onPageDetailsComplete, onError);
			}
			
			function onPageDetailsComplete(response) {
				vm.pageDetails = response;
				loading.hideLoadingOverlay();
				console.log('Got Page Details');
				console.log(vm.pageDetails);
	        }

			function onError(err){
	            vm.error = 'Sorry Mario, but the princess is in another castle =( ...';
				console.log(vm.error);
			}

		}

})();