# Lexfa App #

App made for the Lexfa Football community

### What is this repository for? ###
* Inside this app one can see the most recent news about the league, the game calendars, statistics about each team, the teams roster and image galleries from the most recent games
* This app was made using ionic and angular as the base, and it's available for IOS and Android
* Version 1.0
* [Visite the league site](http://www.lexfa.mx/)
